export default {
    setTransmitterGroupsData(state, data){
        state.transmitterGroupsData = data;
    },

    setTransmitterListData(state, data){
        state.transmitterListData = data;
    }
}