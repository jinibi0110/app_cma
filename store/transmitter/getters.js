export default {
    getTransmitterGroupsData(state){
        return state.transmitterGroupsData;
    },

    getTransmitterListData(state){
        return state.transmitterListData
    }
}