import axios from 'axios';
import { isArray } from 'util';

export default {

  async fetchTransmitterGroups({commit}, {user_id, user_actions, token}){
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/user-groups/transmitter-groups`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      if(Array.isArray(res.data.fetched)){
        commit('setTransmitterGroupsData', res.data.fetched);
      }
    }).catch(err => {
      console.log(err.response);
    });
  },

  async fetchTransmitters({commit}, {user_id, user_actions, token, id}){
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/user/user-group/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      if(Array.isArray(res.data.fetched)){
        commit('setTransmitterListData', res.data.fetched);
      }
    }).catch(err => {
      console.log(err.response);
    }); 
  }

}