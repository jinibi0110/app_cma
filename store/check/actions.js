import axios from 'axios';

export default {
    
    // Fetch staled cheques main table
    async fetchStaledMainTableData({commit}, {user_id, user_actions, token}){
        const staledTableChecksArr = [];

        await axios({
        method: "GET",
        url:`${this.$axios.defaults.baseURL}/check/staled`,
        headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
            user_id,
            user_actions,
        }
        }).then(res => {
            console.log(res.data)
        if(Array.isArray(res.data.fetched))
            res.data.fetched.forEach(item => {
                staledTableChecksArr.push(item);
            })

        }).catch(err => {
        console.log(err)
        });
        commit('setStaledMainTableData', staledTableChecksArr)
    }
  
}