import axios from 'axios';

export default {

  //reset transmit fields
  resetTransmit({commit}){
    const transmit = {
      details: 
      {
        transmittalNumber: null,
        organization: null,
        releasingLocation: null,
        description: null,
        actualCount: 0,
        actualAmount: 0,
        isPosted: null,
        transmittalDate: null,
        transmittedByName: null,
        receivedByName: null,
        releasingLocationName: null,
        transmittingGroup: null
      },
        checks: []
    };
     commit('setTransmitTransactionDetails', transmit);
  },

    // fetch transfer main table
    async fetchTransferData({commit}, { user_actions, token }){
      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}/transaction/transferred`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
        if (Array.isArray(res.data.fetched))
          commit('setTransferData', res.data.fetched);
          const transferArray = [];
          res.data.fetched.forEach(element => {
              const data = {
                id: element.id,
                transactionNumber: element.transferNumber,
                description: element.description,
                controlCount: element.controlCount,
                controlAmount: element.controlAmount,
                isPosted: element.isPosted,
                transactionDate: element.transferDate,
                checkStatus: 'transferred'
              }
              transferArray.push(data);
          });
          commit('setTransmitMainTableData', transferArray);
        }
      ).catch(err => console.log(err.response))
    },

    // Create Transaction (Cheque Writer)
    async createTransaction({ commit }, {user_id, user_actions, token, transactionFields}) {
      await axios({
          method: 'POST',
          url: `${this.$axios.defaults.baseURL}/check/transfer`,
          headers: {
            Authorization: `Bearer ${token}`
          },
          data: {
            user_id,
            user_actions,
            ...transactionFields
          }
      }).then(res => {
        commit('addCreatedTransaction', res.data)
      }).catch(err => console.log(err.response));
    },
    
    // Fetch Accounts Payable cheques from SAP modal table
    async fetchTransferModalData({commit}, { user_actions, token }){
      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}/check/get/accounts-payable`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
        if (Array.isArray(res.data.fetched))
          commit('setTransferModalData', res.data.fetched)
        }
      ).catch(err => console.log(err.response))
    },

    async fetchVoidData({commit}, { user_actions, token }){
      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}/check/void`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
        if (Array.isArray(res.data.fetched))
          commit('setVoidData', res.data.fetched)
        }
      ).catch(err => console.log(err.response))
    },
    
    // fetch transmit main table
    async fetchTransmitData({commit}, { user_actions, token }){
      const transmitTableArr = [];

      await axios({
        method: 'PUT',
          url: `${this.$axios.defaults.baseURL}/transaction/get-to-receive`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions,
          transaction: 'transferredTransactions'
        }
      }).then(res => {
        if (Array.isArray(res.data.fetched.transactions))
          commit('setTransferData', res.data.fetched.transactions);

          res.data.fetched.transactions.forEach(element => {
              const data = {
                id: element.id,
                transactionNumber: element.transferNumber,
                description: element.description,
                controlCount: element.controlCount,
                controlAmount: element.controlAmount,
                isPosted: element.isPosted,
                transactionDate: element.transferDate,
                checkStatus: 'transferred'
              }

              transmitTableArr.push(data);
          });

        }
      ).catch(err => console.log(err.response))

      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}/transaction/transmit`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
          if (Array.isArray(res.data.fetched)){
            res.data.fetched.forEach(element => {
                const data = {
                  id: element.id,
                  transactionNumber: element.transmittalNumber,
                  description: element.description,
                  controlCount: element.controlCount,
                  controlAmount: element.controlAmount,
                  isPosted: element.isPosted,
                  transactionDate: element.transmittalDate,
                  checkStatus: 'transmitted'
                }

                transmitTableArr.push(data);
            });
          };
        }).catch(err => console.log(err.response));

        commit('setTransmitMainTableData', transmitTableArr);
    },

    async receiveTransferredTransaction({commit}, {user_id, user_actions, token, transactionId, receivedBy}){
      await axios({
        method: "PUT",
        url: `${this.$axios.defaults.baseURL}/transaction/transferred/receive-transaction/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          receivedBy
        }
      }).then(res => {
        // console.log(res.data);
      }).catch(err => {
        console.log(err)
      });
    },

    async fetchTransferredChecks({commit}, {user_actions, token}){
      await axios({
        method: "GET",
        url: `${this.$axios.defaults.baseURL}/check/transferred`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched)){
          commit('setTransferredChecksData', res.data.fetched);
        }
      }).catch(err => {
        console.log(err)
      })
    },

    async transmitTransaction({ commit }, { user_id, user_actions, token,  transmitTransactionFields}) {
      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/check/transmit`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          ...transmitTransactionFields
        }
      }).then(res => {
        commit('addTransmitTransaction', res.data)
      }).catch(err => console.log(err.response));
    },

    async fetchReceivedTransactionNumbers({commit}, {user_id, user_actions, token, transaction}){
      await axios({
        method: 'PUT',
        url: `${this.$axios.defaults.baseURL}/transaction/get-received`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          transaction
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched.transactions)){
          commit('setReceivedTransactionNumbers', res.data.fetched.transactions)
        }
      }).catch(err => console.log(err.response));
    },

    async fetchTransferTransactionDetails({commit}, {user_id, user_actions, token, transactionId}){
      await axios({
        method: 'GET',
        url: `${this.$axios.defaults.baseURL}/transaction/transferred/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions
        }
      }).then(res => {
        commit('setTransferTransactionDetails', res.data.fetched)
      }).catch(err => console.log(err.response));
    },

    async fetchReleasingLocation({commit}, {user_id, user_actions, token, }){
      await axios({
        method: 'GET',
        url: `${this.$axios.defaults.baseURL}/locations/get-all`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
         
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched)){
          commit('setReleasingLocation', res.data.fetched)
        }
      }).catch(err => console.log(err.response));
    },

    async fetchReceiveMainTableData({commit}, {user_id, user_actions, token}){
      const receiveDataArr = [];

      await axios({
        method: 'PUT',
          url: `${this.$axios.defaults.baseURL}/transaction/get-to-receive`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions,
          transaction: 'transmittalTransactions'
        }
      }).then(res => {
        if (Array.isArray(res.data.fetched.transactions))
          res.data.fetched.transactions.forEach(element => {
              const data = {
                id: element.id,
                transactionNumber: element.transmittalNumber,
                description: element.description,
                controlCount: element.controlCount,
                controlAmount: element.controlAmount,
                isPosted: element.isPosted,
                transactionDate: element.transmittalDate,
                checkStatus: 'transmitted'
              }

              receiveDataArr.push(data);
          });

        }
      ).catch(err => console.log(err.response));

      await axios({
        method: 'GET',
          url: `${this.$axios.defaults.baseURL}/transaction/receive`,
          headers: {
            Authorization: `Bearer ${token}`
        },
        data: {
          user_actions
        }
      }).then(res => {
          if (Array.isArray(res.data.fetched)){
            res.data.fetched.forEach(element => {
                const data = {
                  id: element.id,
                  transactionNumber: element.receiveNumber,
                  description: element.description,
                  controlCount: element.controlCount,
                  controlAmount: element.controlAmount,
                  isPosted: element.isPosted,
                  transactionDate: element.receiveDate,
                  checkStatus: 'received'
                }

                receiveDataArr.push(data);
            });
          };
        }).catch(err => console.log(err.response));

        commit('setReceiveMainTableData', receiveDataArr);
    },

    async fetchTransmitTransactionDetails({commit}, {user_id, user_actions, token, transactionId}){     
      await axios({
        method: "GET",
        url:`${this.$axios.defaults.baseURL}/transaction/transmit/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
        }
      }).then(res => {
        if(res.data.fetched.transaction){
          // console.log(res.data.fetched.transaction)
          commit('setTransmitTransactionDetails', res.data.fetched.transaction);
        }
      }).catch(err => {
        console.log(err)
      });
    },

    async receiveTransmittedTransaction({commit}, {user_id, user_actions, token, transactionId, receivedBy}){
      await axios({
        method: "PUT",
        url:`${this.$axios.defaults.baseURL}/transaction/transmitted/receive-transaction/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          receivedBy
        }
      }).then(res => {
        // console.log(res.data);
      }).catch(err => {
        console.log(err)
      });
    },

    async createReceiveTransaction({commit}, {user_id, user_actions, token, receiveTransactionFields}) {
      await axios({
        method: "POST",
        url:`${this.$axios.defaults.baseURL}/check/receive`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          ...receiveTransactionFields
        }
      }).then(res => {
        // console.log(res.data)
      }).catch(err => {
        console.log(err)
      });
    },

    // Fetch release main table
    async fetchReleaseTableMainData({commit}, {user_id, user_actions, token}){
      const releaseTableChecksArr = [];

      await axios({
        method: "GET",
        url:`${this.$axios.defaults.baseURL}/check/received`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched)){
          res.data.fetched.forEach(item => {
            releaseTableChecksArr.push(item);
          });
        }
      }).catch(err => {
        console.log(err)
      });

      await axios({
        method: "GET",
        url:`${this.$axios.defaults.baseURL}/check/released`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched)){
          res.data.fetched.forEach(item => {
            releaseTableChecksArr.push(item);
          });
        }
      }).catch(err => {
        console.log(err)
      });
      commit('setReleaseTableMainData', releaseTableChecksArr);
    },

    async releaseCheck({commit}, {user_id, user_actions, token, releaseDetails}){

      await axios({
        method: "PUT",
        url:`${this.$axios.defaults.baseURL}/check/release`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          ...releaseDetails
        }
      }).then(res => {
        // console.log(res.data)
      }).catch(err => {
        console.log(err)
      });

    },

    async returnCheckDocs({commit}, {user_id, user_actions, token, checkDocsReturnFields}){

      await axios({
        method: "POST",
        url:`${this.$axios.defaults.baseURL}/check/return-check-docs`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          ...checkDocsReturnFields
        }
      }).then(res => {
        // console.log(res.data);
      }).catch(err => {
        console.log(err);
      });

    },

    // Fetch return main table
    async fetchReturnMainTableData({commit}, {user_id, user_actions, token}){
      const transactionsArr = [];

      await axios({
        method: "PUT",
        url:`${this.$axios.defaults.baseURL}/transaction/get-to-receive`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          transaction: 'docsReturnTransactions'
        }
      }).then(res => {
        if(Array.isArray(res.data.fetched.transactions))
          res.data.fetched.transactions.forEach(item => {
            const data = {
              id: item.id,
              transactionNumber: item.returnNumber,
              description: item.description,
              controlCount: item.controlCount,
              controlAmount: item.controlAmount,
              isPosted: item.isPosted,
              transactionDate: item.returnDate,
              checkStatus: 'returned'
            }
            transactionsArr.push(data);
          });
        
      }).catch(err => {
        console.log(err)
      });
      commit('setReturnMainTableData', transactionsArr);
    },

    async fetchReturnTransactionDetails({commit}, {user_id, user_actions, token, transactionId}){
      await axios({
        method: "GET",
        url:`${this.$axios.defaults.baseURL}/transaction/check-docs-return/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions
        }
      }).then(res => {
        if(res.data.fetched.transaction){
          commit('setReturnTransactionDetails', res.data.fetched.transaction);
        }
      }).catch(err => {
        console.log(err)
      });
    },

    async receiveReturningTransaction({commit}, {user_id, user_actions, token, transactionId, receivedBy}){
      await axios({
        method: "PUT",
        url: `${this.$axios.defaults.baseURL}/transaction/check-docs-return/receive-transaction/${transactionId}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          receivedBy
        }
      }).then(res => {
        console.log(res.data);
      }).catch(err => {
        console.log(err)
      });
    },

    async makeReturnReceiptTransaction({ commit }, { user_id, user_actions, token,  returnTransactionFields}) {
      await axios({
        method: 'POST',
        url: `${this.$axios.defaults.baseURL}/check/return-receipt`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          user_id,
          user_actions,
          ...returnTransactionFields
        }
      }).then(res => {
        console.log("gfdgdfgdfgdg",res.data)
      }).catch(err => console.log(err.response));
    },
}