export default {
    getTransferData(state){
        return state.transferData
    },
    getTransferModalData(state){
        return state.transferModalData
    },
    getVoidData(state){
        return state.voidData
    },
    getUserDetails(state){
        return state.userDetails
    },
    getTransmitMainTableData(state){
        return state.transmitMainTableData;
    },
    getTransferredChecksData(state){
        return state.transferredChecksData;
    },
    getReceivedTransactionNumbers(state){
        return state.receivedTransactionNumbers
    },
    getTransferTransactionDetails(state){
        return state.transferTransactionDetails
    },
    getReleasingLocation(state){
        return state.releasingLocation
    },
    getReceiveMainTableData(state){
        return state.receiveMainTableData;
    },
    getTransmitTransactionDetails(state){
        return state.transmitTransactionDetails;
    },
    getReleaseTableMainData(state){
        return state.releaseTableMainData;
    },
    getReturnMainTableData(state){
        return state.returnMainTableData;
    },
    getReturnTransactionDetails(state){
        return state.returnTransactionDetails;
    },
   
    // getTransferredTransactionData(state){
    //     return state.transferredTransactionData
    // }
}