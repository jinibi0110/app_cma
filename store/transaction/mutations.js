export default {
    setTransferData(state,data){     
        state.transferData = data
    },
    setTransferModalData(state,data){
        state.transferModalData = data
    },

    setTransmitMainTableData(state, data){
        state.transmitMainTableData = data
    },

    setVoidData(state,data){
        state.voidData = data
    },
    setUserDetails(state, data){
        state.userDetails = data;
    },

    addCreatedTransaction(state, transactionData) {
        state.transferModalData.push(transactionData);
    },

    addTransmitTransaction(state, transactionData) {
        state.transferredChecksData.push(transactionData);
    },

    setTransferredChecksData(state, data){
        state.transferredChecksData = data
    },

    setReceivedTransactionNumbers(state, data){
        state.receivedTransactionNumbers = data
    },

    setReleasingLocation(state, data){
        state.releasingLocation = data
    },

    setTransferTransactionDetails(state, data){
        state.transferTransactionDetails = data;
    },

    setReceiveMainTableData(state, data){
        state.receiveMainTableData = data
    },

    setTransmitTransactionDetails(state, data){
        state.transmitTransactionDetails = data;
    },

    setReleaseTableMainData(state, data){
        state.releaseTableMainData = data
    },

    setReturnMainTableData(state, data){
        state.returnMainTableData = data
    },

    setReturnTransactionDetails(state, data){
        state.returnTransactionDetails = data;
    },

    

    // setTransferredTransactionData(state, data){
    //     // console.log('mutate', data)
    //     state.transferredTransactionData = data;
    // }
}