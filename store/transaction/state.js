export default {
    transferData:[],
    transferModalData:[],
    voidData:[],
    userDetails:{},
    transmitMainTableData: [],
    transferredChecksData: [],
    receivedTransactionNumbers: [],
    transferTransactionDetails: {
        transaction: {
            details: {
                description: null
            },
            checks: []
        }
    },
    releasingLocation: [],
    receiveMainTableData: [],
    transmitTransactionDetails: {
        details: {
            transmittalNumber: null,
            organization: null,
            releasingLocation: null,
            description: null,
            actualCount: 0,
            actualAmount: 0,
            isPosted: null,
            transmittalDate: null,
            transmittedByName: null
        },
        checks: []
    },
    releaseTableMainData: [],
    returnMainTableData: [],
    returnTransactionDetails: {
        details: {
            returnNumber: null,
            description: null,
            returnDate: null,
            actualCount: 0,
            actualAmount: 0,
            isPosted: null,
            returnedBy: null,
            returnedByName: null,
            returnToName: null,
            returnTo: null
        },
        checks: []
    },
    // transferredTransactionData: {}
}